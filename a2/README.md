> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369: Extensible Enterprise Solutions

## Miciaha Ivey

### Assignment 2 Requirements:

*Three Parts:*

1. Backward-Engineer (using Python) the payroll overtime calculator
2. The program should be organized with two modules(See Ch. 4): 
    1. functions.py module contains the following functions:
        1. get_requirements()
        2. calculate_payroll()
        3. print_pay
    2. main.py module imports the functions.py module, and calls the functions. 
3. Test program using both IDLE and Visual Studio Code


#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshots of of Payroll calculator with overtime
* Screenshots of of Payroll calculator without overtime


#### Assignment Screenshots:

*Screenshot of Payroll Calulator running via Terminal with Overtime*:

![Terminal - Payroll Calculator With Overtime Screenshot](./img/terminal_overtime.PNG)

*Screenshot of Payroll Calulator running via Terminal without Overtime*:

![Terminal - Payroll Calculator Without Overtime Screenshot](./img/terminal_noovertime.PNG)

*Screenshot of Payroll Calulator running via IDLE with Overtime*:

![IDLE - Payroll Calculator With Overtime Screenshot](./img/idle_ot.png)

*Screenshot of Payroll Calulator running via IDLE without Overtime*:

![IDLE - Payroll Calculator Without Overtime Screenshot](./img/idle_noOt.png)