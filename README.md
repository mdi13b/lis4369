> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369: Extensible Enterprise Solutions

## Miciaha Ivey

### Lis 4369 Requirements:

*Course Work Links:*

1. [A1 README.MD](./a1/README.md)
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket turorial (bitbucketstationlocations)
    - Provide Git command descriptions
2. [A2 README.MD](./a2/README.md)
    - Backward-engineer Payroll Calculator using screenshots
    - Test program using both IDLE and Visual Studio Code
3. [A3 README.MD](./a3/README.md)
    - TBD
4. [A4 README.MD](./a4/README.md)
    - TBD
5. [A5 README.MD](./a5/README.md)
    - TBD
6. [P1 README.MD](./p1/README.md)
    - TBD
7. [P2 README.MD](./p2/README.md)
    - TBD