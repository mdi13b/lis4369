> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369: Extensible Enterprise Solutions

## Miciaha Ivey

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
    a) this assignment and
    b)the completed turorial ([bitbucketstationlocations](https://bitbucket.org/miciahai/bitbucketstationlocations/src/master/))

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* git commands w/ short descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - intializes a git repository
2. git status - allows you to see which changes have been staged and which files aren't being tracked
3. git add - adds a change in the working directory to the staging area
4. git commit - saves changes to the local repo
5. git push - uploads local repo content to a remote repo
6. git pull - fetches and downloads content from remote repo to local repo
7. git branch - allows you to work with different branches in remote and local repos

#### Assignment Screenshots:

*Screenshot of Tip Calculator running in IDLE*:

![Tip Calculator IDLE Screenshot](img/idle.png)

*Screenshot of Tip Calculator running via Terminal*:

![Tip Calculator Terminal Screenshot](img/terminal.png)
